// время выполнения запроса = время поиска + время ожидания нужного сектора + время чтения или записи
// время поиска = время перехода с одной дорожки на соседнюю (5мс) * на количество дорожек между текущей и нужной + 1)

// время ожидания = время простоя головки над нужной дорожкой в ожидании начала запрощенного сектора
// время простая = (необходимое положение головки (в градусах) - текующее положение головки (в градусах)) * скорость вращение (относительно градусов) 
// необходимое положение головки = 360 / общее количество секторов * номер запрощенного сектора
// текущее положение головки = (время прошедшее от начала работы диска * скорость диска) % 360
// время прощедшее от начала работы диска до начала времени простоя = суммарное время выполнения каждого запроса + сумма интервалов между запросами


// время чтения = количество считываемых секторов * скорость чтения одного сетктора
// время записи = количество записываемых секторов * скорость чтения одного сетктора + время за которое диск совершает полный оброт

// скорость вращения диска = 1 оборот \ 6 мс = 60 градусов \ 1 мс
// скорость чтения одного сектора =  время за которое диск совершает полный оброт \ количество секторов 

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <time.h>
#include <locale.h>
#include <string.h>



struct Query {
    int first_sector;  // Сектора нумеруются от 0 до disk.sectors - 1
    int last_sector; 
    int track_number; 
    int surface_number; 
    char* operation_type; 
    double receipt_time;
    double query_execution_time; 
};

void printQuery(struct Query query) {
    printf("Query:\nfirst_sector = %d\nlast_sector = %d\ntrack_number = %d\nsurface_number = %d\noperation_type = %s\nreceipt_time = %lf\nquery_execution_time=%lf\n", 
    query.first_sector, query.last_sector, query.track_number, query.surface_number, query.operation_type, query.receipt_time, query.query_execution_time);
}

void generateQueryParams(struct Query *query, double receipt_time) {
    query->first_sector = rand() % 16; // 0 - (disk.sectors - 1)
    query->last_sector = rand() % 16;
    query->track_number = rand() % 500;
    query->surface_number = rand() % 4;
    query->receipt_time = receipt_time;

    if (rand() % 2 == 0) 
        query->operation_type = "w";
    else
        query->operation_type = "r";

    query->query_execution_time = -1;
}


void execute_query(struct Query *querys, int querysBufferLengh) {

    printf("size of query buffer: %d\n", querysBufferLengh);
    for (unsigned int i = 0; i < querysBufferLengh; i++) {
//        printQuery(querys[i]);
    }
    struct Query *selectedQuery;
    selectedQuery = &querys[select_query_fifo(querys, querysBufferLengh)];
    printf("%lf\n\n", selectedQuery->receipt_time);
    selectedQuery->receipt_time = 1234.1234;
    printf("%lf\n\n", selectedQuery->receipt_time);
}

int select_query_fifo(struct Query *querysBuffer, int querysBufferLengh) {
    for (unsigned int i = 0; i < querysBufferLengh; i++) {
        if (i == 10) {
            printf("Query in select func:\n");
            printQuery(querysBuffer[i]);
            return i;
        }
    }
}
    

//int get_element(int *array){
//    return array[1];
//}
//
//int exe_element(int *array){
//    int element = get_element(&array);
//    element = 1023;
//}

int main() {
//
//    int array[10];
//    int element = 0;
//
//    for (int i = 0; i < 10; i++){
//        array[i] = i;
//    }
//
//    element = array[1];
//    printf("%d ", element);
//
//    int element2 = get_element(&array);
//
//    printf("%d ", element2);



    setlocale(LC_ALL, "Rus");
    srand(time(NULL));

    int max_query_iterval_time = 100;
    double simulation_time = 5 * 60000;

    int queryBufferLengh = 100;
    struct Query querysBuffer[queryBufferLengh];

    double time = 0;
    int counter = 0;
    while (time < simulation_time) {
        if (counter >= queryBufferLengh) {
            break; // Потом при выполнении запросов нужно проверять не закончились ли эти запросы
                    // Также необходимо учитывать то что буфер может быть не доконца сгенерирован, так как условие по времени выполнилось быстрее
        }
        time += rand() % max_query_iterval_time + 1;
        generateQueryParams(&querysBuffer[counter++], time);
         printf("\ntime = %lf\n", time);
//         printQuery(querysBuffer[counter]);
    }

    execute_query(&querysBuffer, queryBufferLengh);
    printQuery(querysBuffer[10]);

//    unsigned int currentQueryIndex = 0;
//    while (disk.time < simulation_time) {
//        if (currentQueryIndex++ > queryBufferLengh) {
//            printf("Буфер сгенерированных запросов полностью обслужен. Время симуляции %lf\n", disk.time);
//            break;
//        }
//        execute_query(&disk, 'sstf', &querysBuffer, &stats);
//    }
    
    // printStats(stats, disk, &querysBuffer);
    
    return 0;
}