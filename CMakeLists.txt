cmake_minimum_required(VERSION 3.16)
project(c_project C)

set(CMAKE_C_STANDARD 99)

add_executable(c_project main_orig.c)